# iOS Assignment CS

You should build an application using [TheMovieDB API](https://developers.themoviedb.org/3). We have provided an initial application that will help you with fast-tracking app development. It contains the following:

* Class containing **baseUrl** and **API Key** which is needed to fetch contents from TMDB API.
* Basic implementation of a scrollable list to fetch contents and display them in list format.
* Basic JSON parsing to parse server response and populate details.
* Placeholder RatingView class

## Delivering the code
* Fork this repo and select the access level as private **[Check how to do it here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)**
* Go to settings and add the user **m-cs-recruitment@backbase.com** with the read access **[Check how to do it here](https://confluence.atlassian.com/bitbucket/grant-repository-access-to-users-and-groups-221449716.html)**
* Send an e-mail to **m-cs-recruitment@backbase.com** with your info and repo once the development is done

Please remember to work with small commits, it help us to see how you improve your code :)

## Technical Information
We expect you to implement the following functionalities in the app:


1. List horizontally currently playing movies.
Only display poster images in the horizontal scrolling list view.
*No pagination necessary.*
```API: https://api.themoviedb.org/3/movie/now_playing?language=en-US&page=undefined&api_key=55957fcf3ba81b137f8fc01ac5a31fb5```


2. Display the most popular movies in the vertical listview, as this list will contain multiple pages, Pagination support will be required.
```API: https://api.themoviedb.org/3/movie/popular?api_key=55957fcf3ba81b137f8fc01ac5a31fb5&language=en-US&page=1```

    * Each list item will contain the following:
    * Poster image
    * Title
    * Rating
    * Release date

3. Layouts to follow:
[Here are links](https://app.abstract.com/share/8cb87be4-4250-45e0-9066-abcdf4d5dd79) to the layouts you need to follow, you can inspect each view to get the dimensions

4. When a user clicks on any movie list item, it will navigate to a detailed screen, with more information about the movie.
```API: https://api.themoviedb.org/3/movie/{MOVIE_ID}?api_key={YOUR_KEY}```
Detail screen should contain the following information:
    * Poster image
        **[Use the API as per described](https://developers.themoviedb.org/3/getting-started/images)**
    * Duration
    * Title
    * Overview
    * Release date
    * List of genres

## Additional requirements / restrictions
Provide a README.md explaining your approach to solve the image caching also the custom progressbar implementation and any other important decision you took or assumptions you made during the implementation.

Minimum Supported versions:
**iOS - 13.0 +**
